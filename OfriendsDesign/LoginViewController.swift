//
//  LoginViewController.swift
//  OfriendsDesign
//
//  Created by Tudor Tise on 13/07/2019.
//  Copyright © 2019 Tudor Tise. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet var email: UITextField!
    
    @IBOutlet var password: UITextField!
    
    let greenColor : UIColor = UIColor(red: 0.24, green: 0.77, blue: 0.63, alpha: 1)
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hideKeyboardWhenTappedAround()
        
        email.layer.borderWidth = 1.0
        email.layer.borderColor = greenColor.cgColor
        
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 30, height: self.email.frame.height))
        email.leftView = paddingView
        email.leftViewMode = UITextField.ViewMode.always
        
        
        let paddingView2 = UIView(frame: CGRect(x: 0, y: 0, width: 30, height: self.password.frame.height))
        password.leftView = paddingView2
        password.leftViewMode = UITextField.ViewMode.always
        password.layer.borderWidth = 1.0
        password.layer.borderColor = greenColor.cgColor

    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
