//
//  TabBarController.swift
//  
//
//  Created by Tudor Tise on 03/11/2019.
//

import UIKit

class TabBarController: UITabBarController {

    var name = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let finalVC = self.viewControllers![0] as! HomeViewController
        
        finalVC.nameLabel = name
        
    }
    

}
