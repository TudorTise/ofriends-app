//
//  BecomeViewController.swift
//  OfriendsDesign
//
//  Created by Tudor Tise on 13/07/2019.
//  Copyright © 2019 Tudor Tise. All rights reserved.
//

import UIKit
import Firebase

class BecomeViewController: UIViewController {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        if #available(iOS 13.0, *) {
            return .darkContent
        } else {
            // Fallback on earlier versions
            return .default
        }
    }
    
    
    @IBOutlet var fullName: UITextField!
    @IBOutlet var email: UITextField!
    
    
 
    var username = ""
    var emailDB = ""
    
    var ref: DatabaseReference!

    
    
    private var datePick : UIDatePicker?
    
    let greenColor : UIColor = UIColor(red: 0.24, green: 0.77, blue: 0.63, alpha: 1)
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
          ref = Database.database().reference()
        
        
        fullName.layer.cornerRadius = 10.0
        fullName.layer.borderWidth = 1.0
        fullName.layer.borderColor = greenColor.cgColor
        
        fullName.attributedPlaceholder = NSAttributedString(string: "Nume", attributes: [NSAttributedString.Key.foregroundColor : UIColor.lightGray])
        
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 30, height: self.fullName.frame.height))
        
        paddingView.backgroundColor = UIColor.white
        
        fullName.leftView = paddingView
        fullName.leftViewMode = UITextField.ViewMode.always
        
        
        
        let paddingView2 = UIView(frame: CGRect(x: 0, y: 0, width: 30, height: self.email.frame.height))
        
        paddingView2.backgroundColor = UIColor.white
        
        email.layer.cornerRadius = 10.0
        email.layer.borderWidth = 1.0
        email.layer.borderColor = greenColor.cgColor
        email.leftView = paddingView2
        email.leftViewMode = UITextField.ViewMode.always
        
        email.attributedPlaceholder = NSAttributedString(string: "Email", attributes: [NSAttributedString.Key.foregroundColor : UIColor.lightGray])
       
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(BecomeViewController.viewTapped))
        
        view.addGestureRecognizer(tap)
        
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(swipeAction5(swipe:)))
        rightSwipe.direction = UISwipeGestureRecognizer.Direction.right
        self.view.addGestureRecognizer(rightSwipe)
        
    }
    
    
    @IBAction func sendData(_ sender: Any) {
        
        if fullName.text != "" {
            
                //username = fullName.text!
            //self.ref.child("users").child(fullName.text!).setValue(["username": username])
            
            
            if email.text != ""{
                
                username = fullName.text!
                
                self.ref.child("users").child(fullName.text!).setValue(["username": username])
             
                emailDB = email.text!
                
                self.ref.child("users").child(fullName.text!).setValue(["email": emailDB])
                
                
                email.text = ""
                fullName.text = ""
                
                //ALERT
                /*
                let alert3 = UIAlertController(title: "Gata", message: " Datele dumneavoastră au fost trimise cu succes", preferredStyle: .alert)
               
                
                
                self.present(alert3, animated: true, completion: nil)
                
                let when = DispatchTime.now() + 2
                DispatchQueue.main.asyncAfter(deadline: when){
                    alert3.dismiss(animated: true, completion: nil)
 
                }*/
                
            
                
                
                
                self.performSegue(withIdentifier: "goHome", sender: self)
                
                
                
                
            } else {
                print("Error la Email")
                
                let alert = UIAlertController(title: "Atenție", message: "Nu ați introdus nicio adresă de email", preferredStyle: .alert)

                self.present(alert, animated: true, completion: nil)
                
                alert.addAction(UIAlertAction(title: NSLocalizedString("Am înțeles", comment: "Default action"), style: .default))
                
                
            }
            
        } else {
            
            let alert2 = UIAlertController(title: "Atenție", message: "Nu ați introdus niciun nume", preferredStyle: .alert)
            
            self.present(alert2, animated: true, completion: nil)
            
            alert2.addAction(UIAlertAction(title: NSLocalizedString("Am înțeles", comment: "Default action"), style: .default))
            
            print("Error la FullName")
        }
        
        
        
        
       
        
        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
       
        let vc = segue.destination as! TabBarController
        
        
        vc.name = self.username
    }
 
 
 
    
    
    
    
    @objc func viewTapped(gestureRecognizer : UITapGestureRecognizer){
        view.endEditing(true)
        
        
    }
    


}

extension UIViewController {
    
    @objc func swipeAction5(swipe : UISwipeGestureRecognizer){
        
        switch swipe.direction.rawValue{
        case 1:
            performSegue(withIdentifier: "goLeft4", sender: self)
        default:
            break
        }
    }
}







