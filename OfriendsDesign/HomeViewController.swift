//
//  HomeViewController.swift
//  Ofriends
//
//  Created by Tudor Tise on 02/11/2019.
//  Copyright © 2019 Tudor Tise. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        if #available(iOS 13.0, *) {
            return .darkContent
        } else {
            // Fallback on earlier versions
            return .default
        }
    }

    @IBOutlet var topMessage: UILabel!
    
    @IBOutlet var eventButton: UIButton!
    
    @IBAction func evButton(_ sender: Any) {
        
        self.tabBarController?.selectedIndex = 1
        
    
    }
    
    var nameLabel = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        topMessage.text = "Salut " + nameLabel  + " cum ești?"
        
        eventButton.layer.cornerRadius = 20
        eventButton.clipsToBounds = true
        
        
    }
    

}
