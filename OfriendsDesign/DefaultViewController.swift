//
//  DefaultViewController.swift
//  OfriendsDesign
//
//  Created by Tudor Tise on 16/09/2019.
//  Copyright © 2019 Tudor Tise. All rights reserved.
//

import UIKit

class DefaultViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

       
    }
    
    
    
    @IBAction func buttonPressAjuta(_ sender: Any) {
        
        if let url = URL(string: "https://ofriends.typeform.com/to/c0ZoMT") {
            if #available(iOS 10, *){
                UIApplication.shared.open(url)
            }else{
                UIApplication.shared.openURL(url)
            }
            
        }
        
    }
    @IBAction func buttonPressShare(_ sender: Any) {
        
        /*let alert = UIAlertController(title: "Feature comming soon", message: "", preferredStyle: .alert)
        
        self.present(alert, animated: true, completion: nil)
        
        let when = DispatchTime.now() + 2
        DispatchQueue.main.asyncAfter(deadline: when){
            alert.dismiss(animated: true, completion: nil)
            
        */
        
        let activityShare = UIActivityViewController(activityItems: ["ofriends.info"], applicationActivities: nil)
               
               activityShare.popoverPresentationController?.sourceView = self.view
               
               self.present(activityShare, animated: true, completion: nil)
        
        
        }
        
    }
    


