//
//  HelpViewController.swift
//  OfriendsDesign
//
//  Created by Tudor Tise on 16/09/2019.
//  Copyright © 2019 Tudor Tise. All rights reserved.
//

import UIKit

class HelpViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(swipeAction3(swipe:)))
        
        leftSwipe.direction = UISwipeGestureRecognizer.Direction.left
        self.view.addGestureRecognizer(leftSwipe)
        
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(swipeAction3(swipe:)))
        rightSwipe.direction = UISwipeGestureRecognizer.Direction.right
        self.view.addGestureRecognizer(rightSwipe)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        if #available(iOS 13.0, *) {
            return .darkContent
        } else {
            // Fallback on earlier versions
            return .default
        }
    }
}

extension UIViewController {
    
    @objc func swipeAction3(swipe : UISwipeGestureRecognizer){
        
        switch swipe.direction.rawValue{
        case 1:
            performSegue(withIdentifier: "goLeft2", sender: self)
        case 2:
            performSegue(withIdentifier: "goRight3", sender: self)
        default:
            break
        }
    }
}
