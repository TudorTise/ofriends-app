//
//  ViewController.swift
//  OfriendsDesign
//
//  Created by Tudor Tise on 12/07/2019.
//  Copyright © 2019 Tudor Tise. All rights reserved.
//

import UIKit

//now it works

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
   
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(swipeAction(swipe:)))
        leftSwipe.direction = UISwipeGestureRecognizer.Direction.left
        self.view.addGestureRecognizer(leftSwipe)
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        if #available(iOS 13.0, *) {
            return .darkContent
        } else {
            // Fallback on earlier versions
            return .default
        }
    }

}


extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}


extension UIViewController{
    
    @objc func swipeAction(swipe : UISwipeGestureRecognizer){
        
        switch swipe.direction.rawValue{
        case 2:
            performSegue(withIdentifier: "goRight1", sender: self)
        default:
            break
        }
    }
    
}
